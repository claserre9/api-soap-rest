# Système d'information distribué par la gestion des matchs d'échecs

## Problématique

La **_fédération québécoise des échecs_**, en raison du nombre croissant de joueurs d’échecs et du nombre de visiteurs affluant sur son site, souhaite rendre accessible ses ressources sur d’autres plateformes (application de bureau et mobile). Cette situation augmente aussi les charges au niveau de la gestion des joueurs et des matchs. Donc, étendre les services et les ressources permettra non seulement de les décentraliser mais surtout d’en attribuer une partie de la gestion aux clubs d’échecs affiliés. Pour mieux répondre à ce besoin d’extension, l’approche appropriée sera de développer un système d’information distribué sous forme de microservices (API SOAP et REST).

## Identification des acteurs

- Les clubs affiliés
- La fédération québécoise des échecs

## Cas d'utilisation

**Microservice de gestion des joueurs par les clubs**

- Consulter la liste des joueurs
- Consulter un joueur Ajouter un joueur
- Modifier un joueur
- Supprimer un joueur

**Microservice de gestion des matchs d’échecs par la fédération**

- Consulter tous les matchs
- Voir les informations d’un match
- Ajouter un match
- Modifier un match
- Supprimer un match

_Remarque : Ajouter, modifier et supprimer un match doivent être des opérations privées à la fédération qui ne vont pas être partagées._

## Description du projet de développement

Une API REST et une API SOAP seront développées et reliées à une base de données contenant les données provenant d'une table comprenant les joueurs des clubs et autre table comportant les résultats des matchs disputés par les joueurs. Une application Androïd sera aussi développée comme client afin de permettre au club de gérer leur joueur et à la fédération de gérer les résultats des matchs entre les joueurs.

**Dans le cadre de ce projet, seulement le microservice de gestion des joueurs d'échecs sera développé**
